from django.urls import path

from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.home, name='home'),
    
    path('buku/<str:book_id>', views.buku, name="buku"),
    
]

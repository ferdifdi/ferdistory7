from django.test import TestCase, LiveServerTestCase
from django.urls import resolve, reverse
from .views import *

# Create your tests here.

class UrlsTest(TestCase):

    def setUp(self):
        
        book_id = "12345",
    
        self.home = reverse("story8:home")
        self.buku = reverse("story8:buku", args=[book_id])
    
    def test_home_function(self):
        found = resolve(self.home)
        self.assertEqual(found.func, home)

    def test_buku_function(self):
        found = resolve(self.buku)
        self.assertEqual(found.func, buku)

class ViewsTest(TestCase):

    def setUp(self):
        
        book_id = "12345",
    
        self.home = reverse("story8:home")
        self.buku_exist = reverse("story8:buku", args=[book_id])
        self.buku_not_exist = reverse("story8:buku", args=['12345'])


    def test_GET_home(self):
        response = self.client.get(self.home)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story8/home.html")

    def test_GET_buku(self):
        response_exist = self.client.get(self.buku_exist)
        self.assertEqual(response_exist.status_code, 200)
        self.assertJSONEqual(response_exist.content, {'books' : 0})
        response_not_exist = self.client.get(self.buku_not_exist)
        self.assertEqual(response_not_exist.status_code, 200)
        self.assertJSONEqual(response_not_exist.content, {'books' : 0})
  
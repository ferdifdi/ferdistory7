from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm

# Create your views here.

@login_required(login_url='story9:login')
def home(request):
    return render(request, 'story9/home.html')

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('story9:home')
    else:
        form = UserCreationForm()

    context = {'form' : form}
    return render(request, 'registration/register.html', context)

# def login_view(request):
#     username = request.POST['username']
#     password = request.POST['password']
#     user = authenticate(request, username=username, password=password)
#     if user is not None:
#         login(request, user)
#         # Redirect to a success page.
#         return redirect('story9:login')
        
#     else:
#         messages.info(request, 'Username or password is incorrect')
#         # Return an 'invalid login' error message.
#     context = {}
#     return render(request, 'story9/login.html', context)

# def register_view(request):
#     pass

def logout_view(request):
    logout(request)
    # Redirect to a success page.
    return redirect('story9:home')

from django.urls import path, include

from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.home, name='home'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('register/', views.register, name='register'),
    path('logout/', views.logout_view, name='logout'),
]

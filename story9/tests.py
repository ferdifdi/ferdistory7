from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from .views import *
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
# from django.contrib.auth.views import LoginView


# Create your tests here.

class ModelTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(
            username = 'ferdifdi',
            password = 'inikoderahasia123',
        )

    def test_instance_created(self):
        self.assertEqual(User.objects.count(), 1)
        
class UrlsTest(TestCase):

    def setUp(self):
        self.home = reverse("story9:home")
        self.login = reverse("story9:login")
        self.register = reverse("story9:register")
        self.logout = reverse("story9:logout")
    
    def test_home_use_right_function(self):
        found = resolve(self.home)
        self.assertEqual(found.func, home)

    # def test_login_use_right_function(self):
    #     found = resolve(self.login)
    #     self.assertEqual(found.func, LoginView)

    def test_register_use_right_function(self):
        found = resolve(self.register)
        self.assertEqual(found.func, register)

    def test_logout_use_right_function(self):
        found = resolve(self.logout)
        self.assertEqual(found.func, logout_view)

class ViewsTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.home = reverse("story9:home")
        self.login = reverse("story9:login")
        self.register = reverse("story9:register")
        self.logout = reverse("story9:logout")

    def test_GET_home_no_data(self):
        # If no one login it redirect to login page
        response1 = self.client.get(self.home, follow=True)
        self.assertEqual(response1.status_code, 200)
        self.assertIn("login", str(response1.content))
        # If a user login it show her name
        user = User.objects.create(username='ferdifdi')
        user.set_password('inikoderahasia123')
        user.save()
        self.client.login(username='ferdifdi', password='inikoderahasia123')        
        response2 = self.client.get(self.home)
        self.assertEqual(response2.status_code, 200)
        self.assertTemplateUsed(response2, "story9/home.html")
        self.assertIn("ferdifdi", str(response2.content))

    def test_GET_home_data_exist(self):
        # If no one login it redirect to login page
        response1 = self.client.get(self.home, follow=True)
        self.assertEqual(response1.status_code, 200)
        self.assertIn("login", str(response1.content))
        # If a user login it show her name
        user = User.objects.create(username='ferdifdi')
        user.set_password('inikoderahasia123')
        user.save()
        self.client.login(username='ferdifdi', password='inikoderahasia123')    

    def test_GET_register(self):
        response = self.client.get(self.register)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "registration/register.html")
        self.assertIn("register", str(response.content))

    def test_POST_register_invalid(self):
        response = self.client.post(self.register, {
            'username': 'ferdifdi',
            'password1': 'ew',
            'password2': 'ew'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "registration/register.html")
        self.assertIn("This password is too short. It must contain at least 8 characters.", str(response.content))

    def test_POST_register_valid(self):
        response = self.client.post(self.register, {
            'username': 'ferdifdi',
            'password1': 'inikoderahasia123',
            'password2': 'inikoderahasia123'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story9/home.html")
        self.assertIn("This is the home page!", str(response.content))

    def test_GET_login(self):
        response = self.client.get(self.login)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "registration/login.html")
        # idk

    def test_POST_login_invalid(self):
        response = self.client.post(self.login, {
            'username': 'ferdifdi',
            'password': 'pwsalah',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "registration/login.html")

    def test_POST_login_valid(self):
        user = User.objects.create(username='ferdifdi')
        user.set_password('inikoderahasia123')
        user.save()
        response = self.client.post(self.login, {
            'username': 'ferdifdi',
            'password': 'inikoderahasia123',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story9/home.html")
        self.assertIn("ferdifdi", str(response.content))

    def test_GET_logout(self):
        # If a user login, this view should logout the user and redirect to loginpage
        user = User.objects.create(username='ferdifdi')
        user.set_password('inikoderahasia123')
        user.save()
        self.client.login(username='ferdifdi', password='inikoderahasia123')      
        response = self.client.get(self.logout, follow=True) 
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "registration/login.html")
        self.assertIn("Please login to see this page.", str(response.content))

# class FunctionalTest(LiveServerTestCase):

#     def setUp(self):
#         super().setUp()
#         chrome_options = Options()
#         chrome_options.add_argument("--window-size=1920,1080");
#         chrome_options.add_argument('--dns-prefectch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')

#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

#     def tearDown(self):
#         self.selenium.quit()
#         super().tearDown()
    
#     def test_register_invalid(self):
#         selenium = self.selenium
#         wait = WebDriverWait(selenium, 5)
#         # opening the link we want to test
#         selenium.get(self.live_server_url+'/story9/register')
#         # find the register input and button
#         username = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_username"]')))
#         password1 = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_password1"]')))
#         password2 = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_password2"]')))
#         register = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-register')))
#         # fill the input
#         username.send_keys("ferdifdi")
#         password1.send_keys("ew")
#         password2.send_keys("ew")
#         # click the register button
#         register.click()
#         # wait for a while
#         time.sleep(1)
#         # check error message exist
#         self.assertIn("This password is too short", selenium.page_source)

    # def test_register_valid_login_logout(self):
    #     selenium = self.selenium
    #     wait = WebDriverWait(selenium, 5)
    #     # opening the link we want to test
    #     selenium.get(self.live_server_url+'/story9/register')
    #     # find the register input and button
    #     username = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_username"]')))
    #     password1 = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_password1"]')))
    #     password2 = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_password2"]')))
    #     register = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-register')))
    #     # fill the input
    #     username.send_keys("ferdifdi")
    #     password1.send_keys("inikoderahasia123")
    #     password2.send_keys("inikoderahasia123")
    #     # click the register button
    #     register.click()
    #     # wait until redirected to login page (the login input and button exist)
    #     login_username = wait.until(EC.element_to_be_clickable((By.XPATH, '/div/div/form/table/tr[0]/td[1]/input'))) #bingung euy
    #     login_password = wait.until(EC.element_to_be_clickable((By.XPATH, '/div/div/form/table/tr[1]/td[1]/input'))) #bingung euy
    #     login_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-login')))
    #     # fill the input
    #     login_username.send_keys("ferdifdi")
    #     login_password.send_keys("inikoderahasia123")
    #     login_button.click()
    #     # It should redirected to index
    #     # wait until logout button exist
    #     logout_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-logout')))
    #     # Check name of the user exist
    #     self.assertIn("ferdifdi", selenium.page_source)
    #     # logout
    #     logout_button.click()
    #     # It should redirected back to login
    #     # Check login_button exist
    #     wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-logout')))
    #     self.assertIn("Login", selenium.page_source)

    
